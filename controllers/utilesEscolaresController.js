const { guardarDB, leerDB } = require('../helpers/guardar');
const Util = require('../models/util');
const Utiles = require('../models/utiles');

const { response } = 'express';

// instanciamos Personas, lo hacemos global para que se añada un nuevo usuario con cada petición POST
const utiles = new Utiles();

// Método para obtener una persona
// GET //
const utilGet = (req = request, res = response) => {
  
  const utilDB = leerDB();

  if (utilDB) {
    utiles.cargarUtilFromArray(utilDB);
  }

  res.json({
    msg: 'get API - Controlador',
    // Mostramos aqui la lista de personas
    utilDB
  })

};

// PUT //
// Método para actulizar una persona
const utilPut = (req, res = response) => {

  const { id } = req.params

  if (id) {
    utiles.eliminarUtil(id)
  
    const { material, precio_unitario, precio_total } = req.body;
    // pasamos los parámetros de Persona
    const util = new Util(material, precio_unitario, precio_total);
  
    util.setID(id)
  
    utiles.crearUtil(util);
    guardarDB(utiles.listArr);
  }

  res.json({
    msg: 'put API - Controlador'
  })
}

// POST //
// Método para añadir una persona 
const utilPost = (req, res = response) => {
  const { material, precio_unitario, precio_total } = req.body;

  // pasamos los parámetros de Persona
  const util = new Util(material, precio_unitario, precio_total);

  let utilDB = leerDB()
  // instanciamos Personas
  // const personas = new Personas();

  // condicion para cargar los datos ya guardados
  if (utilDB) {
    utiles.cargarUtilFromArray(utilDB);
  }
  // añadimos una nueva 'persona' a 'personas'
  utiles.crearUtil(util);
  guardarDB(utiles.listArr);
  utilDB = leerDB()

  const listado = leerDB();

  if (listado) {
    utiles.cargarUtilFromArray(listado);
  }

  res.json({
    msg: 'post API - Controlador',
    listado
  })

}

// DELETE //
// Método para eliminar una persona
const utilDelete = (req, res = response) => {

  const { id } = req.params
  
  if (id) {
    utiles.eliminarUtil(id);
    console.log("Este es delete:",utiles.listArr);
    guardarDB(utiles.listArr)
    // console.log(leerDB());
  }

  res.json({
    msg: 'delete API - Controlador',
  })
};


module.exports = {
  utilGet,
  utilPut,
  utilPost,
  utilDelete
}