const Util = require("./util");

class Utiles {
  constructor() {
    this._listado = [];
  }

  // Método para crear persona
  crearUtil(util = {}) {

    this._listado[util.id] = util;
  }


  // getter para recorrer los posts de personas y almacenarlos, va junto con crearPersona()
  get listArr() {
    const listado = [];
    Object.keys(this._listado).forEach(key => {
      const util = this._listado[key];
      listado.push(util);
    })

    return listado;
  }


  // Método para mostrar la lista de personas en el parámetro GET
  cargarUtilFromArray(utiles = []) {
    utiles.forEach(util => {
      this._listado[util.id] = util;
    })
  }

  
  // Método para eliminar una persona
  eliminarUtil(id = '') {
    if (this._listado[id]) {
      // console.log(this._listado[id]);
      delete this._listado[id];
    }
  }
}

module.exports = Utiles;